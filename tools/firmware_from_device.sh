#!/bin/bash

#############################################################
# Extract firmware from device and process it for usage.
#
# Usage: bash tools/firmware_from_device.sh <build_directory>
#
# Requirements:
#  - adb
#  - bash
#  - pil-squasher - https://github.com/andersson/pil-squasher
#############################################################

BUILD_DIR=${1:-./build}
if [ ! -d "${BUILD_DIR}" ]; then
  mkdir "${BUILD_DIR}"
fi

TMP_DIR=$(mktemp -d "${BUILD_DIR}/XXXXXXXXX")

echo "[+] Creating and entering temporary directory -> ${TMP_DIR}"
pushd "${TMP_DIR}" &> /dev/null || (echo "[!] Failed to create temporary directory" && exit)

echo "[+] Starting adb as root"
adb root

# Check if adb actually restarted as root
if [ "$(adb shell whoami)" != "root" ]; then
  echo "[!] Could not start adb as root, please check your device"
  exit 1
fi

echo "[+] Pulling firmware"
adb pull /vendor/firmware
adb pull /vendor/firmware_mnt

echo "[+] Firmware version:"
cat "firmware_mnt/verinfo/ver_info.txt"
echo ""

echo "[+] Squashing firmware using pil-squasher"
pil-squasher "a630_zap.mbn" "firmware/a630_zap.mdt"
pil-squasher "ipa_fws.mbn"  "firmware/ipa_fws.mdt"

pil-squasher "adsp.mbn"  "firmware_mnt/image/adsp.mdt"
pil-squasher "cdsp.mbn"  "firmware_mnt/image/cdsp.mdt"
pil-squasher "modem.mbn" "firmware_mnt/image/modem.mdt"
pil-squasher "slpi.mbn"  "firmware_mnt/image/slpi.mdt"
pil-squasher "venus.mbn" "firmware_mnt/image/venus.mdt"

# Disabled - see: https://gitlab.com/sdm845-mainline/firmware-shift-sdm845/-/commit/ce9d27bf03624cceeaa8e7466f2e6dc731f2ba5e
#echo "[+] Copying firmware files"
#cp "firmware_mnt/image/mba.mbn"      "mba.mbn"
#cp "firmware_mnt/image/wlanmdsp.mbn" "wlanmdsp.mbn"

echo "[+] Copying jsn files"
cp "firmware_mnt/image/adspr.jsn"   "adspr.jsn"
cp "firmware_mnt/image/adspua.jsn"  "adspua.jsn"
cp "firmware_mnt/image/cdspr.jsn"   "cdspr.jsn"
cp "firmware_mnt/image/modemr.jsn"  "modemr.jsn"
cp "firmware_mnt/image/modemuw.jsn" "modemuw.jsn"
cp "firmware_mnt/image/slpir.jsn"   "slpir.jsn"
cp "firmware_mnt/image/slpius.jsn"  "slpius.jsn"

echo "[+] Cleaning up firmware files"
rm -rf "firmware"
rm -rf "firmware_mnt"

popd &> /dev/null || (echo "[!] Failed to exit temporary directory" && exit)

echo "[+] Moving files to build directory"
mv -v "${TMP_DIR}"/* "${BUILD_DIR}"

echo "[+] Cleaning up temporary directory"
rm -rf "${TMP_DIR}"

echo ""
echo "[+] Done, please check '${BUILD_DIR}' and have a nice day! ≽(•ᴗ•)≼"
exit 0
